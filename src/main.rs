use rss::Channel;
use std::env;
use std::fs::OpenOptions;
use std::io::Read;
use std::io::Write;

fn main() {
    // Get news
    let channel =
        Channel::from_url("https://www.archlinux.org/feeds/news/").expect("invalid channel");

    let latest_item = &channel.items()[0];

    // Read chached news
    let home_path = env::var("HOME").expect("error parsing home variable");
    let file_path = format!("{}/.cache/arch-news-feed", home_path);

    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(&file_path)
        .expect("Problem opening file");

    // let mut file = match file {
    //     Ok(file) => file,
    //     Err(error) => match error.kind() {
    //         io::ErrorKind::NotFound => match File::create(file_path) {
    //             Ok(file_created) => file_created,
    //             Err(error) => panic!("Problem creating the file: {:?}", error),
    //         },
    //         other_error => panic!("Problem opening the file: {:?}", other_error),
    //     },
    // };

    let mut latest_item_id = String::new();

    file.read_to_string(&mut latest_item_id)
        .expect("failed to read file");
    let guid_value = latest_item.guid().expect("invalid guid").value();

    if latest_item_id == guid_value {
        println!("No news available");
    } else {
        file.write_all(guid_value.as_bytes())
            .expect("failed to write file");

        match latest_item.title() {
            Some(title) => println!("{}", title),
            None => println!("There is no title."),
        }
    }
}
